import { h, render } from "preact";
const OnlineUsers = ({ users }) => {
  const onlineCount = 340;
  const membersCount = 75432;

  return (
    <div className="online-users-container">
      <div className="profile-images-container">
        {users.slice(0, 4).map((user, index) => (
          <div
            key={index}
            className="profile-image bg-orange-400"
            style={{ marginLeft: index !== 0 ? "-9px" : "0" }}
          >
            <div className="profile-image-border">
              <img src={user.image} alt={`User ${index + 1}`} />
            </div>
          </div>
        ))}
      </div>
      <div className="status-info">
        <div className="status-item">
          <div className="dot green-dot"></div>
          <span>{onlineCount} online</span>
        </div>
        <div className="status-item">
          <div className="dot grey-dot"></div>
          <span>{membersCount} members</span>
        </div>
      </div>
    </div>
  );
};

export default OnlineUsers;
