const path = require("path");
const esbuild = require("esbuild");
const { writeFileSync } = require("fs");

const result = esbuild.buildSync({
  entryPoints: {
    embed: path.join(__dirname, "./index.js"),
  },
  bundle: true,
  loader: {
    ".js": "jsx",
    ".css": "text", // We're using this to embed CSS in the preact code
  },
  jsxFactory: "h",
  jsxFragment: "Fragment",
  write: false,
  minify: true,
});
const code = new TextDecoder("utf-8").decode(result.outputFiles[0].contents);
writeFileSync(path.join(__dirname, "./source.js"), `${code}`);
