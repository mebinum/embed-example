import { h, render } from "preact";
import { useState } from "preact/hooks";
import styles from "./embed.css";
import OnlineUsers from "./onlineusers";

const users = [
  {
    id: 1,
    image: "https://robohash.org/hicveldicta.png?size=50x50&set=set1",
  },
  {
    id: 2,
    image:
      "https://robohash.org/doloremquesintcorrupti.png?size=50x50&set=set1",
  },
  {
    id: 3,
    image:
      "https://robohash.org/consequunturautconsequatur.png?size=50x50&set=set1",
  },
  {
    id: 4,
    image:
      "https://robohash.org/facilisdignissimosdolore.png?size=50x50&set=set1",
  },
  {
    id: 5,
    image: "https://robohash.org/laboriosamfacilisrem.png?size=50x50&set=set1",
  },
];

const Embed = () => {
  const [tooltipVisible, setTooltipVisible] = useState(false);
  const [isOpen, setisOpen] = useState(false);

  setTimeout(() => setTooltipVisible(true), 1000);

  const toggleIframe = () => {
    setisOpen(!isOpen);
  };

  return (
    <div id="example">
      <style dangerouslySetInnerHTML={{ __html: styles }} />

      <iframe
        frameborder="0"
        class={`example-frame ${isOpen ? "open" : ""}`}
        src="https://usehall.com/" // ___FRAME_URL___ will be replace on server
      ></iframe>

      <div class="example-wrapper">
        <div>
          <button
            class={`example-button ${tooltipVisible && !isOpen ? "" : "off"}`}
            onClick={toggleIframe}
          >
            <OnlineUsers users={users} />
          </button>

          <div
            class={`example-tooltip ${tooltipVisible && !isOpen ? "" : "off"}`}
          >
            <div class="example-message desktop">
              <span class="line1">Join the discussion </span>
              <span class="line2">
                You get instant answers to your questions
              </span>
            </div>
            <div class="example-message mobile">
              <span class="line1">Join the chat</span>
            </div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 256 512"
              class="example-caret"
            >
              <path d="M118.6 105.4l128 127.1C252.9 239.6 256 247.8 256 255.1s-3.125 16.38-9.375 22.63l-128 127.1c-9.156 9.156-22.91 11.9-34.88 6.943S64 396.9 64 383.1V128c0-12.94 7.781-24.62 19.75-29.58S109.5 96.23 118.6 105.4z" />
            </svg>
          </div>
        </div>
      </div>
      {isOpen && (
        <button class="example-close" onClick={toggleIframe}>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
            <path d="M310.6 361.4c12.5 12.5 12.5 32.75 0 45.25C304.4 412.9 296.2 416 288 416s-16.38-3.125-22.62-9.375L160 301.3L54.63 406.6C48.38 412.9 40.19 416 32 416S15.63 412.9 9.375 406.6c-12.5-12.5-12.5-32.75 0-45.25l105.4-105.4L9.375 150.6c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 210.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-105.4 105.4L310.6 361.4z" />
          </svg>
        </button>
      )}
    </div>
  );
};

const load = () => {
  const embed = document.createElement("div");
  document.body.appendChild(embed);
  render(<Embed />, embed);
};

if (document.readyState !== "loading") {
  load();
} else {
  document.addEventListener("DOMContentLoaded", load);
}
