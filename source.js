(()=>{var V,p,oe,Ee,H,X,_e,I,Se,T={},re=[],He=/acit|ex(?:s|g|n|p|$)|rph|grid|ows|mnc|ntw|ine[ch]|zoo|^ord|itera/i,$=Array.isArray;function S(e,t){for(var n in t)e[n]=t[n];return e}function ie(e){var t=e.parentNode;t&&t.removeChild(e)}function d(e,t,n){var _,i,r,a={};for(r in t)r=="key"?_=t[r]:r=="ref"?i=t[r]:a[r]=t[r];if(arguments.length>2&&(a.children=arguments.length>3?V.call(arguments,2):n),typeof e=="function"&&e.defaultProps!=null)for(r in e.defaultProps)a[r]===void 0&&(a[r]=e.defaultProps[r]);return L(e,a,_,i,null)}function L(e,t,n,_,i){var r={type:e,props:t,key:n,ref:_,__k:null,__:null,__b:0,__e:null,__d:void 0,__c:null,__h:null,constructor:void 0,__v:i??++oe};return i==null&&p.vnode!=null&&p.vnode(r),r}function O(e){return e.children}function M(e,t){this.props=e,this.context=t}function U(e,t){if(t==null)return e.__?U(e.__,e.__.__k.indexOf(e)+1):null;for(var n;t<e.__k.length;t++)if((n=e.__k[t])!=null&&n.__e!=null)return n.__d||n.__e;return typeof e.type=="function"?U(e):null}function se(e){var t,n;if((e=e.__)!=null&&e.__c!=null){for(e.__e=e.__c.base=null,t=0;t<e.__k.length;t++)if((n=e.__k[t])!=null&&n.__e!=null){e.__e=e.__c.base=n.__e;break}return se(e)}}function Z(e){(!e.__d&&(e.__d=!0)&&H.push(e)&&!z.__r++||X!==p.debounceRendering)&&((X=p.debounceRendering)||_e)(z)}function z(){var e,t,n,_,i,r,a,c,l;for(H.sort(I);e=H.shift();)e.__d&&(t=H.length,_=void 0,i=void 0,r=void 0,c=(a=(n=e).__v).__e,(l=n.__P)&&(_=[],i=[],(r=S({},a)).__v=a.__v+1,j(l,a,r,n.__n,l.ownerSVGElement!==void 0,a.__h!=null?[c]:null,_,c??U(a),a.__h,i),ce(_,a,i),a.__e!=c&&se(a)),H.length>t&&H.sort(I));z.__r=0}function ae(e,t,n,_,i,r,a,c,l,x,m){var o,h,u,s,f,N,v,y,C,w=0,b=_&&_.__k||re,A=b.length,E=A,P=t.length;for(n.__k=[],o=0;o<P;o++)(s=n.__k[o]=(s=t[o])==null||typeof s=="boolean"||typeof s=="function"?null:typeof s=="string"||typeof s=="number"||typeof s=="bigint"?L(null,s,null,null,s):$(s)?L(O,{children:s},null,null,null):s.__b>0?L(s.type,s.props,s.key,s.ref?s.ref:null,s.__v):s)!=null?(s.__=n,s.__b=n.__b+1,(y=Ne(s,b,v=o+w,E))===-1?u=T:(u=b[y]||T,b[y]=void 0,E--),j(e,s,u,i,r,a,c,l,x,m),f=s.__e,(h=s.ref)&&u.ref!=h&&(u.ref&&q(u.ref,null,s),m.push(h,s.__c||f,s)),f!=null&&(N==null&&(N=f),(C=u===T||u.__v===null)?y==-1&&w--:y!==v&&(y===v+1?w++:y>v?E>P-v?w+=y-v:w--:w=y<v&&y==v-1?y-v:0),v=o+w,typeof s.type!="function"||y===v&&u.__k!==s.__k?typeof s.type=="function"||y===v&&!C?s.__d!==void 0?(l=s.__d,s.__d=void 0):l=f.nextSibling:l=pe(e,f,l):l=le(s,l,e),typeof n.type=="function"&&(n.__d=l))):(u=b[o])&&u.key==null&&u.__e&&(u.__e==l&&(u.__=_,l=U(u)),B(u,u,!1),b[o]=null);for(n.__e=N,o=A;o--;)b[o]!=null&&(typeof n.type=="function"&&b[o].__e!=null&&b[o].__e==n.__d&&(n.__d=b[o].__e.nextSibling),B(b[o],b[o]))}function le(e,t,n){for(var _,i=e.__k,r=0;i&&r<i.length;r++)(_=i[r])&&(_.__=e,t=typeof _.type=="function"?le(_,t,n):pe(n,_.__e,t));return t}function pe(e,t,n){return n==null||n.parentNode!==e?e.insertBefore(t,null):t==n&&t.parentNode!=null||e.insertBefore(t,n),t.nextSibling}function Ne(e,t,n,_){var i=e.key,r=e.type,a=n-1,c=n+1,l=t[n];if(l===null||l&&i==l.key&&r===l.type)return n;if(_>(l!=null?1:0))for(;a>=0||c<t.length;){if(a>=0){if((l=t[a])&&i==l.key&&r===l.type)return a;a--}if(c<t.length){if((l=t[c])&&i==l.key&&r===l.type)return c;c++}}return-1}function Pe(e,t,n,_,i){var r;for(r in n)r==="children"||r==="key"||r in t||F(e,r,null,n[r],_);for(r in t)i&&typeof t[r]!="function"||r==="children"||r==="key"||r==="value"||r==="checked"||n[r]===t[r]||F(e,r,t[r],n[r],_)}function ee(e,t,n){t[0]==="-"?e.setProperty(t,n??""):e[t]=n==null?"":typeof n!="number"||He.test(t)?n:n+"px"}function F(e,t,n,_,i){var r;e:if(t==="style")if(typeof n=="string")e.style.cssText=n;else{if(typeof _=="string"&&(e.style.cssText=_=""),_)for(t in _)n&&t in n||ee(e.style,t,"");if(n)for(t in n)_&&n[t]===_[t]||ee(e.style,t,n[t])}else if(t[0]==="o"&&t[1]==="n")r=t!==(t=t.replace(/(PointerCapture)$|Capture$/,"$1")),t=t.toLowerCase()in e?t.toLowerCase().slice(2):t.slice(2),e.l||(e.l={}),e.l[t+r]=n,n?_?n.u=_.u:(n.u=Date.now(),e.addEventListener(t,r?ne:te,r)):e.removeEventListener(t,r?ne:te,r);else if(t!=="dangerouslySetInnerHTML"){if(i)t=t.replace(/xlink(H|:h)/,"h").replace(/sName$/,"s");else if(t!=="width"&&t!=="height"&&t!=="href"&&t!=="list"&&t!=="form"&&t!=="tabIndex"&&t!=="download"&&t!=="rowSpan"&&t!=="colSpan"&&t!=="role"&&t in e)try{e[t]=n??"";break e}catch{}typeof n=="function"||(n==null||n===!1&&t[4]!=="-"?e.removeAttribute(t):e.setAttribute(t,n))}}function te(e){var t=this.l[e.type+!1];if(e.t){if(e.t<=t.u)return}else e.t=Date.now();return t(p.event?p.event(e):e)}function ne(e){return this.l[e.type+!0](p.event?p.event(e):e)}function j(e,t,n,_,i,r,a,c,l,x){var m,o,h,u,s,f,N,v,y,C,w,b,A,E,P,k=t.type;if(t.constructor!==void 0)return null;n.__h!=null&&(l=n.__h,c=t.__e=n.__e,t.__h=null,r=[c]),(m=p.__b)&&m(t);e:if(typeof k=="function")try{if(v=t.props,y=(m=k.contextType)&&_[m.__c],C=m?y?y.props.value:m.__:_,n.__c?N=(o=t.__c=n.__c).__=o.__E:("prototype"in k&&k.prototype.render?t.__c=o=new k(v,C):(t.__c=o=new M(v,C),o.constructor=k,o.render=Ae),y&&y.sub(o),o.props=v,o.state||(o.state={}),o.context=C,o.__n=_,h=o.__d=!0,o.__h=[],o._sb=[]),o.__s==null&&(o.__s=o.state),k.getDerivedStateFromProps!=null&&(o.__s==o.state&&(o.__s=S({},o.__s)),S(o.__s,k.getDerivedStateFromProps(v,o.__s))),u=o.props,s=o.state,o.__v=t,h)k.getDerivedStateFromProps==null&&o.componentWillMount!=null&&o.componentWillMount(),o.componentDidMount!=null&&o.__h.push(o.componentDidMount);else{if(k.getDerivedStateFromProps==null&&v!==u&&o.componentWillReceiveProps!=null&&o.componentWillReceiveProps(v,C),!o.__e&&(o.shouldComponentUpdate!=null&&o.shouldComponentUpdate(v,o.__s,C)===!1||t.__v===n.__v)){for(t.__v!==n.__v&&(o.props=v,o.state=o.__s,o.__d=!1),t.__e=n.__e,t.__k=n.__k,t.__k.forEach(function(D){D&&(D.__=t)}),w=0;w<o._sb.length;w++)o.__h.push(o._sb[w]);o._sb=[],o.__h.length&&a.push(o);break e}o.componentWillUpdate!=null&&o.componentWillUpdate(v,o.__s,C),o.componentDidUpdate!=null&&o.__h.push(function(){o.componentDidUpdate(u,s,f)})}if(o.context=C,o.props=v,o.__P=e,o.__e=!1,b=p.__r,A=0,"prototype"in k&&k.prototype.render){for(o.state=o.__s,o.__d=!1,b&&b(t),m=o.render(o.props,o.state,o.context),E=0;E<o._sb.length;E++)o.__h.push(o._sb[E]);o._sb=[]}else do o.__d=!1,b&&b(t),m=o.render(o.props,o.state,o.context),o.state=o.__s;while(o.__d&&++A<25);o.state=o.__s,o.getChildContext!=null&&(_=S(S({},_),o.getChildContext())),h||o.getSnapshotBeforeUpdate==null||(f=o.getSnapshotBeforeUpdate(u,s)),ae(e,$(P=m!=null&&m.type===O&&m.key==null?m.props.children:m)?P:[P],t,n,_,i,r,a,c,l,x),o.base=t.__e,t.__h=null,o.__h.length&&a.push(o),N&&(o.__E=o.__=null)}catch(D){t.__v=null,(l||r!=null)&&(t.__e=c,t.__h=!!l,r[r.indexOf(c)]=null),p.__e(D,t,n)}else r==null&&t.__v===n.__v?(t.__k=n.__k,t.__e=n.__e):t.__e=Te(n.__e,t,n,_,i,r,a,l,x);(m=p.diffed)&&m(t)}function ce(e,t,n){for(var _=0;_<n.length;_++)q(n[_],n[++_],n[++_]);p.__c&&p.__c(t,e),e.some(function(i){try{e=i.__h,i.__h=[],e.some(function(r){r.call(i)})}catch(r){p.__e(r,i.__v)}})}function Te(e,t,n,_,i,r,a,c,l){var x,m,o,h=n.props,u=t.props,s=t.type,f=0;if(s==="svg"&&(i=!0),r!=null){for(;f<r.length;f++)if((x=r[f])&&"setAttribute"in x==!!s&&(s?x.localName===s:x.nodeType===3)){e=x,r[f]=null;break}}if(e==null){if(s===null)return document.createTextNode(u);e=i?document.createElementNS("http://www.w3.org/2000/svg",s):document.createElement(s,u.is&&u),r=null,c=!1}if(s===null)h===u||c&&e.data===u||(e.data=u);else{if(r=r&&V.call(e.childNodes),m=(h=n.props||T).dangerouslySetInnerHTML,o=u.dangerouslySetInnerHTML,!c){if(r!=null)for(h={},f=0;f<e.attributes.length;f++)h[e.attributes[f].name]=e.attributes[f].value;(o||m)&&(o&&(m&&o.__html==m.__html||o.__html===e.innerHTML)||(e.innerHTML=o&&o.__html||""))}if(Pe(e,u,h,i,c),o)t.__k=[];else if(ae(e,$(f=t.props.children)?f:[f],t,n,_,i&&s!=="foreignObject",r,a,r?r[0]:n.__k&&U(n,0),c,l),r!=null)for(f=r.length;f--;)r[f]!=null&&ie(r[f]);c||("value"in u&&(f=u.value)!==void 0&&(f!==e.value||s==="progress"&&!f||s==="option"&&f!==h.value)&&F(e,"value",f,h.value,!1),"checked"in u&&(f=u.checked)!==void 0&&f!==e.checked&&F(e,"checked",f,h.checked,!1))}return e}function q(e,t,n){try{typeof e=="function"?e(t):e.current=t}catch(_){p.__e(_,n)}}function B(e,t,n){var _,i;if(p.unmount&&p.unmount(e),(_=e.ref)&&(_.current&&_.current!==e.__e||q(_,null,t)),(_=e.__c)!=null){if(_.componentWillUnmount)try{_.componentWillUnmount()}catch(r){p.__e(r,t)}_.base=_.__P=null,e.__c=void 0}if(_=e.__k)for(i=0;i<_.length;i++)_[i]&&B(_[i],t,n||typeof e.type!="function");n||e.__e==null||ie(e.__e),e.__=e.__e=e.__d=void 0}function Ae(e,t,n){return this.constructor(e,n)}function ue(e,t,n){var _,i,r,a;p.__&&p.__(e,t),i=(_=typeof n=="function")?null:n&&n.__k||t.__k,r=[],a=[],j(t,e=(!_&&n||t).__k=d(O,null,[e]),i||T,T,t.ownerSVGElement!==void 0,!_&&n?[n]:i?null:t.firstChild?V.call(t.childNodes):null,r,!_&&n?n:i?i.__e:t.firstChild,_,a),ce(r,e,a)}V=re.slice,p={__e:function(e,t,n,_){for(var i,r,a;t=t.__;)if((i=t.__c)&&!i.__)try{if((r=i.constructor)&&r.getDerivedStateFromError!=null&&(i.setState(r.getDerivedStateFromError(e)),a=i.__d),i.componentDidCatch!=null&&(i.componentDidCatch(e,_||{}),a=i.__d),a)return i.__E=i}catch(c){e=c}throw e}},oe=0,Ee=function(e){return e!=null&&e.constructor===void 0},M.prototype.setState=function(e,t){var n;n=this.__s!=null&&this.__s!==this.state?this.__s:this.__s=S({},this.state),typeof e=="function"&&(e=e(S({},n),this.props)),e&&S(n,e),e!=null&&this.__v&&(t&&this._sb.push(t),Z(this))},M.prototype.forceUpdate=function(e){this.__v&&(this.__e=!0,e&&this.__h.push(e),Z(this))},M.prototype.render=O,H=[],_e=typeof Promise=="function"?Promise.prototype.then.bind(Promise.resolve()):setTimeout,I=function(e,t){return e.__v.__b-t.__v.__b},z.__r=0,Se=0;var Y,g,G,fe,J=0,ye=[],R=[],de=p.__b,me=p.__r,he=p.diffed,ve=p.__c,ge=p.unmount;function Ue(e,t){p.__h&&p.__h(g,e,J||t),J=0;var n=g.__H||(g.__H={__:[],__h:[]});return e>=n.__.length&&n.__.push({__V:R}),n.__[e]}function Q(e){return J=1,De(be,e)}function De(e,t,n){var _=Ue(Y++,2);if(_.t=e,!_.__c&&(_.__=[n?n(t):be(void 0,t),function(c){var l=_.__N?_.__N[0]:_.__[0],x=_.t(l,c);l!==x&&(_.__N=[x,_.__[1]],_.__c.setState({}))}],_.__c=g,!g.u)){var i=function(c,l,x){if(!_.__c.__H)return!0;var m=_.__c.__H.__.filter(function(h){return h.__c});if(m.every(function(h){return!h.__N}))return!r||r.call(this,c,l,x);var o=!1;return m.forEach(function(h){if(h.__N){var u=h.__[0];h.__=h.__N,h.__N=void 0,u!==h.__[0]&&(o=!0)}}),!(!o&&_.__c.props===c)&&(!r||r.call(this,c,l,x))};g.u=!0;var r=g.shouldComponentUpdate,a=g.componentWillUpdate;g.componentWillUpdate=function(c,l,x){if(this.__e){var m=r;r=void 0,i(c,l,x),r=m}a&&a.call(this,c,l,x)},g.shouldComponentUpdate=i}return _.__N||_.__}function Le(){for(var e;e=ye.shift();)if(e.__P&&e.__H)try{e.__H.__h.forEach(W),e.__H.__h.forEach(K),e.__H.__h=[]}catch(t){e.__H.__h=[],p.__e(t,e.__v)}}p.__b=function(e){g=null,de&&de(e)},p.__r=function(e){me&&me(e),Y=0;var t=(g=e.__c).__H;t&&(G===g?(t.__h=[],g.__h=[],t.__.forEach(function(n){n.__N&&(n.__=n.__N),n.__V=R,n.__N=n.i=void 0})):(t.__h.forEach(W),t.__h.forEach(K),t.__h=[],Y=0)),G=g},p.diffed=function(e){he&&he(e);var t=e.__c;t&&t.__H&&(t.__H.__h.length&&(ye.push(t)!==1&&fe===p.requestAnimationFrame||((fe=p.requestAnimationFrame)||Me)(Le)),t.__H.__.forEach(function(n){n.i&&(n.__H=n.i),n.__V!==R&&(n.__=n.__V),n.i=void 0,n.__V=R})),G=g=null},p.__c=function(e,t){t.some(function(n){try{n.__h.forEach(W),n.__h=n.__h.filter(function(_){return!_.__||K(_)})}catch(_){t.some(function(i){i.__h&&(i.__h=[])}),t=[],p.__e(_,n.__v)}}),ve&&ve(e,t)},p.unmount=function(e){ge&&ge(e);var t,n=e.__c;n&&n.__H&&(n.__H.__.forEach(function(_){try{W(_)}catch(i){t=i}}),n.__H=void 0,t&&p.__e(t,n.__v))};var xe=typeof requestAnimationFrame=="function";function Me(e){var t,n=function(){clearTimeout(_),xe&&cancelAnimationFrame(t),setTimeout(e)},_=setTimeout(n,100);xe&&(t=requestAnimationFrame(n))}function W(e){var t=g,n=e.__c;typeof n=="function"&&(e.__c=void 0,n()),g=t}function K(e){var t=g;e.__c=e.__(),g=t}function be(e,t){return typeof t=="function"?t(e):t}var we=`/* ========================================================== WRAPPER ========================================================== */

.example-wrapper {
  bottom: 24px;
  position: fixed;
  right: 24px;
  font-family: Aeonik,sans-serif;
  z-index: 9999;
}

/* ========================================================== FRAME ========================================================== */

.example-frame {
  border: 0;
  border-radius: 8px;
  bottom: 124px;
  box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px,
    rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
  height: 666px;
  height: min(666px, calc(100vh - 150px));
  opacity: 0;
  overflow: hidden;
  position: fixed;
  right: 24px;
  transform: translateY(20px);
  transition: 0.2s;
  width: 375px;
}
/* Fallback for older browsers */
@media only screen and (max-width: 700px) {
  .example-frame {
    height: 444px;
    height: min(666px, calc(100vh - 150px));
  }
}
@media only screen and (max-width: 600px) {
  .example-frame {
    border-radius: 0;
    bottom: unset;
    height: 100vh;
    left: 0;
    right: unset;
    top: 0;
    width: 100vw;
  }
}

.example-frame.open {
  opacity: 1;
  transform: translateY(0px);
}

/* ========================================================== BUTTON ========================================================== */

.example-button {
  background: rgba(255, 255, 255, 0.75) ;
  border: 0;
  border-radius: 3px;
  box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 7px 12px 11px -9px;
  font-size: 14px;
  padding: 7px 14px;
  font-family: Aeonik,segoe ui,
    helvetica neue, helvetica, Ubuntu, roboto, noto, arial, sans-serif;
}

@media only screen and (max-width: 600px) {
  .example-button {
    height: 60px;
    width: 60px;
  }
}

.example-button svg {
  fill: #ffffff;
  position: relative;
  top: 2px;
  height: 40px;
  width: 40px;
}

.example-icon {
  padding-left: 6px;
}

@media only screen and (max-width: 600px) {
  .example-button svg {
    height: 32px;
    width: 32px;
  }

  .example-button.open {
    display: none;
  }
}

/* ========================================================== TOOLTIP ========================================================== */

.example-tooltip {
  background: rgba(239, 90, 60, 0.761);
  border-radius: 8px;
  box-shadow: rgba(239, 90, 60, 0.257) 0px 13px 27px -5px,
    rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
  opacity: 1;
  margin: 0;
  position: absolute;
  right: 2px;
  top: -80%;
  transform: translatey(-50%);
  transition: 0.15s ease-in-out;
  width: 248px;
  padding: 8px 12px 8px 12px;
}

@media only screen and (max-width: 600px) {
  .example-tooltip {
    right: 72px;
    text-align: left;
    max-width: 124px;
  }
}

.example-tooltip.off {
  opacity: 0;
  transform: translate(20px, -50%);
}

@media only screen and (max-width: 600px) {
  .example-tooltip.off {
    display: none;
    opacity: 0;
  }
}

/* ========================================================== CARET ========================================================== */

.example-caret {
  position: absolute;
  top:75%;
  transform: rotate(90deg);
  right: 7px;
  width: 24px;
  height: 48px;
  fill: rgba(239, 90, 60, 0.761);
}

@media only screen and (max-width: 600px) {
  .example-caret {
    right: -8px;
    width: 16px;
    height: 24px;
  }
}

/* ========================================================== CLOSE BUTTON ========================================================== */

.example-close {
  display: none;
}

@media only screen and (max-width: 600px) {
  .example-close {
    background-color: rgba(35, 41, 53, 0.2);
    border: 0;
    border-radius: 100%;
    color: #ffffff;
    display: block;
    height: 32px;
    position: fixed;
    right: 32px;
    top: 16px;
    width: 32px;
    z-index: 9999;
  }

  .example-close svg {
    fill: #ffffff;
    width: 14px;
    height: 14px;
    position: relative;
    top: 2px;
  }

  .example-button.off {
    display: none;
  }
}

/* ========================================================== MESSAGE ========================================================== */

.example-message span {
  display: inline-block;
  font-family: Aeonik,segoe ui,
    helvetica neue, helvetica, Ubuntu, roboto, noto, arial, sans-serif;
  line-height: 1.4;
}

.example-message span.line1 {
  font-size: 16px;
  font-weight: 600;
  color: #ffffff;
  margin: 0;
  padding: 0;
}

.example-message span.line2 {
  font-size: 14px;
  font-weight: 400;
  color: #ffffff;
  margin: 0;
  padding: 0;
}

@media only screen and (max-width: 600px) {
  .example-message.desktop {
    display: none;
  }
}

.example-message.mobile {
  display: none;
}

@media only screen and (max-width: 600px) {
  .example-message.mobile {
    display: block;
  }
}

/*  ================================================================ */

.bg-orange-400 {
    --tw-bg-opacity: 1;
    background-color: rgba(239, 90, 60, 0.761);
}

.online-users-container {
  display: flex;
  align-items: center;
}

.profile-images-container {
  display: flex;
}

.profile-image {
  border-radius: 50px;
  overflow: hidden;
}

.profile-image img {
  width: 50px; /* Adjust the size as needed */
  height: 50px; /* Adjust the size as needed */
  border-color: #ffffff;
  object-fit: cover;
}
.profile-image img:hover {
  transform: translateY(2px);
  transition: 0.15s ease-in-out;
}

.profile-image-border {
  border : 3px solid rgb(249, 249, 249);
  border-radius: 50px;
}

.status-info {
  margin-left: 10px; /* Adjust the margin as needed */
}

.status-item {
  display: flex;
  align-items: center;
  margin-bottom: 5px; /* Adjust the margin as needed */
}

.dot {
  width: 8px;
  height: 8px;
  border-radius: 50%;
  margin-right: 5px; /* Adjust the margin as needed */
}

.green-dot {
  background-color: green;
}

.grey-dot {
  background-color: grey;
}
`;var Fe=({users:e})=>d("div",{className:"online-users-container"},d("div",{className:"profile-images-container"},e.slice(0,4).map((_,i)=>d("div",{key:i,className:"profile-image bg-orange-400",style:{marginLeft:i!==0?"-9px":"0"}},d("div",{className:"profile-image-border"},d("img",{src:_.image,alt:`User ${i+1}`}))))),d("div",{className:"status-info"},d("div",{className:"status-item"},d("div",{className:"dot green-dot"}),d("span",null,340," online")),d("div",{className:"status-item"},d("div",{className:"dot grey-dot"}),d("span",null,75432," members")))),ke=Fe;var Ve=[{id:1,image:"https://robohash.org/hicveldicta.png?size=50x50&set=set1"},{id:2,image:"https://robohash.org/doloremquesintcorrupti.png?size=50x50&set=set1"},{id:3,image:"https://robohash.org/consequunturautconsequatur.png?size=50x50&set=set1"},{id:4,image:"https://robohash.org/facilisdignissimosdolore.png?size=50x50&set=set1"},{id:5,image:"https://robohash.org/laboriosamfacilisrem.png?size=50x50&set=set1"}],Oe=()=>{let[e,t]=Q(!1),[n,_]=Q(!1);setTimeout(()=>t(!0),1e3);let i=()=>{_(!n)};return d("div",{id:"example"},d("style",{dangerouslySetInnerHTML:{__html:we}}),d("iframe",{frameborder:"0",class:`example-frame ${n?"open":""}`,src:"https://usehall.com/"}),d("div",{class:"example-wrapper"},d("div",null,d("button",{class:`example-button ${e&&!n?"":"off"}`,onClick:i},d(ke,{users:Ve})),d("div",{class:`example-tooltip ${e&&!n?"":"off"}`},d("div",{class:"example-message desktop"},d("span",{class:"line1"},"Join the discussion "),d("span",{class:"line2"},"You get instant answers to your questions")),d("div",{class:"example-message mobile"},d("span",{class:"line1"},"Join the chat")),d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 256 512",class:"example-caret"},d("path",{d:"M118.6 105.4l128 127.1C252.9 239.6 256 247.8 256 255.1s-3.125 16.38-9.375 22.63l-128 127.1c-9.156 9.156-22.91 11.9-34.88 6.943S64 396.9 64 383.1V128c0-12.94 7.781-24.62 19.75-29.58S109.5 96.23 118.6 105.4z"}))))),n&&d("button",{class:"example-close",onClick:i},d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 320 512"},d("path",{d:"M310.6 361.4c12.5 12.5 12.5 32.75 0 45.25C304.4 412.9 296.2 416 288 416s-16.38-3.125-22.62-9.375L160 301.3L54.63 406.6C48.38 412.9 40.19 416 32 416S15.63 412.9 9.375 406.6c-12.5-12.5-12.5-32.75 0-45.25l105.4-105.4L9.375 150.6c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 210.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-105.4 105.4L310.6 361.4z"}))))},Ce=()=>{let e=document.createElement("div");document.body.appendChild(e),ue(d(Oe,null),e)};document.readyState!=="loading"?Ce():document.addEventListener("DOMContentLoaded",Ce);})();
